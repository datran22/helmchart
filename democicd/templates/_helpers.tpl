reate a name for the deployment resource. The template can be used like this:
metadata:
  name: {{ include "my-app.fullname" . }}
*/}}
{{- define "my-app.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name -}}
{{- end }}

